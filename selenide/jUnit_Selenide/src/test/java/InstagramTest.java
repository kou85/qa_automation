import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;


/**
 * Selenide task
 * @author Konovalov
 * @since 28.04.19
 */

public class InstagramTest {
    By logo = By.xpath("//h1[contains(@class,'NXVPg')]");
    By refInLoginPage = By.xpath("//a[contains(@href, 'switcher')]");
    By login = By.xpath("//input[@ name=\"username\"]");
    By password = By.xpath("//input[@ name=\"password\"]");
    By enter = By.xpath("//div/button[contains(@class, 'L3NKy')]");
    By mobileNumberOrEmail = By.xpath("//input[@autocomplete='tel']");
    By fullName = By.xpath("//input[@name='fullName']");
    By userName = By.xpath("//input[@name='username']");
    By passwordReg = By.xpath("//input[@name='password']");
    By ButtonReg = By.xpath("//*[@id=\"react-root\"]//div[7]//button");
    By iconAppleStore = By.xpath("//a[contains(@href, 'itunes.apple.com')]");
    By iconGooglePlay = By.xpath("//a[contains(@href, 'play.google.com')]");
    By inputThroughFacebook = By.xpath("//form/div[1]/button");
    By selectLang = By.xpath("//select[@class='hztqj']");

    @BeforeAll
    public static void beforeSuite(){
        Configuration.timeout = 8000;
    }

    @BeforeEach
    public void openUrlAndTest() {
        open("https://www.instagram.com");
        //   assertEquals(url(), "https://www.instagram.com");
    }
    @DisplayName("Open url and check logo, first field")
    @Test
    public void checkOpenLogInMenuAndPresenceFields() {

        $(logo).shouldBe(visible);
        $(refInLoginPage)
                .shouldBe(visible)
                .click();
        $(login).shouldBe(visible);
        $(password).shouldBe(visible);
        $(enter).shouldBe(visible);
        screenshot("Test_1");

    }

    @RepeatedTest(2)
    @Test
    public void checkRegistrationFormAndPresenceFields() {
        $(mobileNumberOrEmail).shouldBe(visible);
        $(fullName).shouldBe(visible);
        $(userName).shouldBe(visible);
        $(passwordReg).shouldBe(visible);
        $(ButtonReg)
                .shouldBe(visible)
                .click();
        screenshot("Test_2");

    }
    @Tag("Click")
    @Test
    public void checkClickableIcons() {

        $(iconAppleStore)
                .shouldBe(visible)
                .click();

        $(iconGooglePlay)
                .shouldBe(visible)
                .click();
        screenshot("Test_3");
    }

    @Test
    public void checkExistUrl() {
        $(refInLoginPage)
                .shouldHave(exist);

        $(inputThroughFacebook)
                .shouldHave(exist);

        screenshot("Test_4");

    }
    @DisplayName("check open login in menu")
    @Test
    public void checkLanguage() {
        $(selectLang).selectOption("Українська");
        $(selectLang).parent().shouldHave(text ("Мова"));
        $(selectLang).selectOption("English");
        $(selectLang).parent().shouldHave(text("language"));
        $(selectLang).selectOption("Русский");
        $(selectLang).parent().shouldHave(text ("Язык"));
        screenshot("Test_4");

    }

    @AfterEach
    public void afterMethod(){
        System.out.println("Something after each test");
    }

    @AfterAll
    public static void afterSuit(){
        System.out.println("Clean up everything");
    }

}
