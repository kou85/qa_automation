import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;
import org.testng.annotations.*;


import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.screenshot;


/**
 * Selenide task
 * @author Konovalov
 * @since 28.04.19
 */

public class InstagramTest {
    By logo = By.xpath("//h1[contains(@class,'NXVPg')]");
    By refInLoginPage = By.xpath("//a[contains(@href, 'switcher')]");
    By login = By.xpath("//input[@ name=\"username\"]");
    By password = By.xpath("//input[@ name=\"password\"]");
    By enter = By.xpath("//div/button[contains(@class, 'L3NKy')]");
    By mobileNumberOrEmail = By.xpath("//input[@autocomplete='tel']");
    By fullName = By.xpath("//input[@name='fullName']");
    By userName = By.xpath("//input[@name='username']");
    By passwordReg = By.xpath("//input[@name='password']");
    By ButtonReg = By.xpath("//*[@id=\"react-root\"]//div[7]//button");
    By iconAppleStore = By.xpath("//a[contains(@href, 'itunes.apple.com')]");
    By iconGooglePlay = By.xpath("//a[contains(@href, 'play.google.com')]");
    By inputThroughFacebook = By.xpath("//form/div[1]/button");
    By selectLang = By.xpath("//select[@class='hztqj']");

    @BeforeSuite
    public void beforeSuite(){
        Configuration.timeout = 8000;
    }
    @BeforeTest
    public void beforeTest(){
        System.out.println("Set up something before test");
    }

    @BeforeMethod
    public void openUrlAndTest() {
        open("https://www.instagram.com");
        //   assertEquals(url(), "https://www.instagram.com");
    }

    @Test (priority = 1, description = "Test1")
    public void checkOpenLogInMenuAndPresenceFields() {

        $(logo).shouldBe(visible);
        $(refInLoginPage)
                .shouldBe(visible)
                .click();
        $(login).shouldBe(visible);
        $(password).shouldBe(visible);
        $(enter).shouldBe(visible);
        screenshot("Test_1");

    }


    @Test (priority = 2, invocationCount = 2)
    public void checkRegistrationFormAndPresenceFields() {
        $(mobileNumberOrEmail).shouldBe(visible);
        $(fullName).shouldBe(visible);
        $(userName).shouldBe(visible);
        $(passwordReg).shouldBe(visible);
        $(ButtonReg)
                .shouldBe(visible)
                .click();
        screenshot("Test_2");

    }

    @Test (priority = 3, threadPoolSize = 5)
    public void checkClickableIcons() {

        $(iconAppleStore)
                .shouldBe(visible)
                .click();

        $(iconGooglePlay)
                .shouldBe(visible)
                .click();
        screenshot("Test_3");
    }

    @Test (priority = 4, enabled = true)
    public void checkExistUrl() {
        $(refInLoginPage)
                .shouldHave(exist);

        $(inputThroughFacebook)
                .shouldHave(exist);

        screenshot("Test_4");

    }

    @Test (dependsOnMethods = {"checkOpenLogInMenuAndPresenceFields"})
    public void checkLanguage() {
        $(selectLang).selectOption("Українська");
        $(selectLang).parent().shouldHave(text ("Мова"));
        $(selectLang).selectOption("English");
        $(selectLang).parent().shouldHave(text("language"));
        $(selectLang).selectOption("Русский");
        $(selectLang).parent().shouldHave(text ("Язык"));
        screenshot("Test_4");

    }

    @AfterMethod
    public void afterMethod(){
        System.out.println("Something after each test");
    }
    @AfterTest
    public void afterTest(){
        System.out.println("Something after test");
    }
    @AfterSuite
    public void afterSuit(){
        System.out.println("Clean up everything");
    }

}